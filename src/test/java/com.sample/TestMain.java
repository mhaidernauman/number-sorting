package com.sample;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.List;

public class TestMain {

    List<Integer> listTestOne;
    List<Integer> listTestResultOne;

    List<Integer> listTestTwo;
    List<Integer> listTestResultTwo;

    public TestMain() {
        listTestOne = Arrays.asList(1, 2, 4, 3, 7, 16);
        listTestResultOne = Arrays.asList(1, 3, 7, 2, 4, 16);

        listTestTwo = Arrays.asList(3,4,2,1,6,7);
        listTestResultTwo = Arrays.asList(3,1,7,4,2,6);
    }

    @Test
    public void testFirstSet() {
        List<Integer> result = Main.sortInputArrayEvenOdd(listTestOne);

        for(int index = 0; index < result.size(); index++) {
            assertEquals(result.get(index), listTestResultOne.get(index));

            System.out.println(result.get(index));
        }
    }

    @Test
    public void testSecondSet() {
        List<Integer> result = Main.sortInputArrayEvenOdd(listTestTwo);

        for(int index = 0; index < result.size(); index++) {
            assertEquals(result.get(index), listTestResultTwo.get(index));

            System.out.println(result.get(index));
        }
    }
}
