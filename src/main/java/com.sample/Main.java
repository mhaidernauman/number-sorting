package com.sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        String[] sScannedItems = scanner.nextLine().split(" ");

        List<Integer> inputItems = new ArrayList<>();

        for(String item : sScannedItems)
            inputItems.add(Integer.valueOf(item));

        List<Integer> output = sortInputArrayEvenOdd(inputItems);

        for(int result : output)
            System.out.println(result);
    }

    public static List<Integer> sortInputArrayEvenOdd(List<Integer> inputItems) {
        List<Integer> evenArrayList = new ArrayList<>();
        List<Integer> oddArrayList = new ArrayList<>();

        for(int item : inputItems)
            if(item % 2 == 0)
                evenArrayList.add(item);
            else
                oddArrayList.add(item);

        List<Integer> resultList = new ArrayList<>();

//        resultList.addAll(oddArrayList.stream().sorted().collect(Collectors.toList()));
//        resultList.addAll(evenArrayList.stream().sorted().collect(Collectors.toList()));

        resultList.addAll(oddArrayList);
        resultList.addAll(evenArrayList);

        return resultList;
    }
}
